#
```sh
touch eslint.config.js
```
```js
export default [
  {
    rules: {
      semi: "error",
      "no-unused-labels": 0,
    },
  },
];
```

# install soft

```sh
sudo snap install nvim --classic
sudo apt-get install curl git ripgrep mercurial make binutils bison gcc build-essential

```

# install fonts

```sh
mkdir -p ~/.local/share/fonts/

pushd /tmp/

curl -sLO https://github.com/ryanoasis/nerd-fonts/releases/download/v3.2.1/Hack.zip
curl -sLO https://github.com/ryanoasis/nerd-fonts/releases/download/v3.2.1/UbuntuMono.zip

unzip -o Hack.zip -d ~/.local/share/fonts/
unzip -o UbuntuMono.zip -d ~/.local/share/fonts/

popd

fc-cache -fv
```

# install nvm ( node version manager )

```sh
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
source ~/.bashrc
nvm install --lts
nvm use default --lts
```

# install requirement for tree-sitter

```sh
npm install tree-sitter-cli neovim
```

# install gvm ( go version manager )

```sh
bash < <(curl -s -S -L https://raw.githubusercontent.com/moovweb/gvm/master/binscripts/gvm-installer)
source /home/san/.gvm/scripts/gvm

gvm install go1.4 -B
gvm use go1.4
export GOROOT_BOOTSTRAP=$GOROOT

gvm install go1.17.13
gvm use go1.17.13
export GOROOT_BOOTSTRAP=$GOROOT

gvm install go1.20
gvm use go1.20
export GOROOT_BOOTSTRAP=$GOROOT

gvm install go1.22.4
gvm use go1.22.4 --default
export GOROOT_BOOTSTRAP=$GOROOT
```

# should be installed for python tools

```sh
sudo apt-get install python3.10-venv

# GoLint
# INFO: https://golangci-lint.run/welcome/install/
curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin v1.59.1
```

# install rust

```sh
# INFO: https://itsfoss.com/install-rust-cargo-ubuntu-linux/
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
