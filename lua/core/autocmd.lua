local TrimWhiteSpaceGrp = vim.api.nvim_create_augroup("TrimWhiteSpaceGrp", {})
vim.api.nvim_create_autocmd("BufWritePre", {
  group = TrimWhiteSpaceGrp,
  pattern = "*",
  command = "%s/\\s\\+$//e",
})

local YankHighlightGrp = vim.api.nvim_create_augroup("YankHighlightGrp", {})
vim.api.nvim_create_autocmd("TextYankPost", {
  group = YankHighlightGrp,
  pattern = "*",
  callback = function()
    vim.highlight.on_yank({
      higroup = "IncSearch",
      timeout = 40,
    })
  end,
})

vim.api.nvim_create_autocmd({ "BufWinEnter" }, {
  pattern = { "*.svelte", "*.html" },
  command = "filetype on | set syntax=html",
})

vim.api.nvim_create_autocmd("BufEnter", {
  callback = function(opts)
    if vim.bo[opts.buf].filetype == "markdown" then
      vim.opt.conceallevel = 2
    end
  end,
})

vim.api.nvim_create_autocmd("BufRead", {
  pattern = { "*.yaml", "*.yml" },
  callback = function()
    if vim.fn.search([[hosts:\|tasks:]], "nw") then
      vim.opt.filetype = "yaml.ansible"
    end
  end,
})

vim.api.nvim_create_autocmd("BufRead", {
  pattern = { "*.go", "*.vue", "*.html" },
  callback = function()
    vim.opt.tabstop = 4
    vim.opt.softtabstop = 4
    vim.opt.shiftwidth = 4
  end,
})

-- moved to treesitter
require("vim.treesitter.query").set(
  "go",
  "injections",
  [[((raw_string_literal) @sql
  (#match? @sql "(ALTER|BEGIN|CALL|COMMENT|COMMIT|CONNECT|CREATE|DELETE|DROP|END|EXPLAIN|EXPORT|GRANT|IMPORT|INSERT|LOAD|LOCK|MERGE|REFRESH|RENAME|REPLACE|REVOKE|ROLLBACK|SELECT|SET|TRUNCATE|UNLOAD|UNSET|UPDATE|UPSERT|WITH|alter|begin|call|comment|commit|connect|create|delete|drop|end|explain|export|grant|import|insert|load|lock|merge|refresh|rename|replace|revoke|rollback|select|set|truncate|unload|unset|update|upsert|with).*")
  (#offset! @sql 0 1 0 -1)) ]]
)
