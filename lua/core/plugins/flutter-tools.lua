return {
  "akinsho/flutter-tools.nvim",
  dependencies = {
    "nvim-lua/plenary.nvim",
    "stevearc/dressing.nvim", -- optional for vim.ui.select
    "Neevash/awesome-flutter-snippets", -- snippets for vim-vsnip !!! Mast be configured in cmp
  },
  opts = {
    flutter_path = "/home/san/snap/flutter/common/flutter/bin/flutter",
    flutter_lookup_cmd = nil,
    settings = {
      enableSnippets = true,
    },
  },
}
