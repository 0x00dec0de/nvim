return {
  "dart-lang/dart-vim-plugin",
  config = function()
    vim.g.dart_format_on_save = 1
  end,
}
