return {
  "nvim-telescope/telescope.nvim",
  tag = "0.1.6",
  dependencies = {
    "nvim-lua/plenary.nvim",
    {
      "nvim-telescope/telescope-fzf-native.nvim",
      build = "make", --= "cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build"
    },
    "nvim-tree/nvim-web-devicons",
  },
  opts = {
    defaults = {
      -- for fzf files need to be added to ~/.ignore file
      file_ignore_patterns = { ".node_modules", ".git", ".svelte-kit", ".dart_tool", ".gradle", ".idea" },
    },
  },
  config = function()
    local telescope = require("telescope")
    telescope.load_extension("fzf")
  end,
}
