return {
  "ray-x/lsp_signature.nvim",
  event = "VeryLazy",
  opts = {
    bind = true,
    always_trigger = true,
    doc_lines = 0,
    handler_opts = {
      border = "rounded",
    },
    hint_enable = true,
    hint_prefix = " ",
  },
  config = function(_, opts)
    require("lsp_signature").setup(opts)
  end,
}
