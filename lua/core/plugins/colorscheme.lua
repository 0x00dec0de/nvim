return {
  "folke/tokyonight.nvim",
  lazy = false,
  priority = 1000,
  opts = {},
  config = function()
    -- local bg = "#011627"
    require("tokyonight").setup({
      -- transparent = true,
      -- styles = {
      --   sidebars = "transparent",
      --   floats = "transparent",
      -- },
      --   style = "night",
      --   on_colors = function(colors)
      --     colors.bg = bg
      --  end,
    })
    vim.cmd([[colorscheme tokyonight]])
  end,
}
