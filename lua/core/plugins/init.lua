return {
  "nvim-lua/plenary.nvim", -- lua function that many plugins use
  "christoomey/vim-tmux-navigator", -- tmux navigator
  "nvim-tree/nvim-web-devicons",
}
