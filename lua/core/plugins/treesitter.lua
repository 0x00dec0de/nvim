return {
  "nvim-treesitter/nvim-treesitter",
  event = { "BufReadPre", "BufNewFile" },
  build = ":TSUpdate",
  dependencies = {
    "windwp/nvim-ts-autotag",
    {
      "nvim-treesitter/nvim-treesitter-context",
      opts = { enable = true, mode = "topline", line_number = true },
    },
    "vrischmann/tree-sitter-templ",
  },
  config = function()
    -- import nvim-treesitter plugin
    local treesitter = require("nvim-treesitter.configs")

    vim.filetype.add({ extension = { templ = "templ" } })

    vim.api.nvim_create_autocmd("FileType", {
      pattern = { "markdown" },
      callback = function()
        require("treesitter-context").disable()
      end,
    })

    -- configure treesitter
    treesitter.setup({ -- enable syntax highlighting
      highlight = {
        enable = true,
      },
      -- enable indentation
      indent = { enable = true },
      -- depricated 
      -- -- enable autotagging (w/ nvim-ts-autotag plugin)
      -- autotag = {
      --   enable = true,
      -- },
      -- ensure these language parsers are installed
      ensure_installed = {
        "bash",
        "c",
        "blueprint",
        "cmake",
        "comment",
        "cpp",
        "css",
        "csv",
        "dart",
        "devicetree",
        "diff",
        "dockerfile",
        "doxygen",
        "erlang",
        "fish",
        "git_config",
        "git_rebase",
        "gitattributes",
        "gitcommit",
        "gitignore",
        "gnuplot",
        "go",
        "gomod",
        "gosum",
        "gotmpl",
        "gowork",
        "gpg",
        "groovy",
        "helm",
        "hjson",
        "hlsplaylist",
        "html",
        "htmldjango",
        "http",
        "hurl",
        "hyprlang",
        "ini",
        "java",
        "javascript",
        "jq",
        "jsdoc",
        "json",
        "json5",
        "jsonnet",
        "kconfig",
        "kotlin",
        "koto",
        "lua",
        "luadoc",
        "make",
        "markdown",
        "markdown_inline",
        "nasm",
        "nix",
        "passwd",
        "perl",
        "php",
        "phpdoc",
        "printf",
        "proto",
        "pug",
        "puppet",
        "python",
        "readline",
        "regex",
        "robot",
        "ruby",
        "rust",
        "scala",
        "scss",
        "sql",
        "ssh_config",
        "strace",
        "svelte",
        "templ",
        "terraform",
        "textproto",
        "tmux",
        "toml",
        "typescript",
        "typespec",
        "udev",
        "vim",
        "vimdoc",
        "vue",
        "xml",
        "yaml",
        "zig",
      },
      incremental_selection = {
        enable = true,
        keymaps = {
          init_selection = "<C-space>",
          node_incremental = "<C-space>",
          scope_incremental = false,
          node_decremental = "<bs>",
        },
      },
    }) -- end setup

    require("vim.treesitter.query").set(
      "go",
      "injections",
      [[((raw_string_literal) @sql
  (#match? @sql "(ALTER|BEGIN|CALL|COMMENT|COMMIT|CONNECT|CREATE|DELETE|DROP|END|EXPLAIN|EXPORT|GRANT|IMPORT|INSERT|LOAD|LOCK|MERGE|REFRESH|RENAME|REPLACE|REVOKE|ROLLBACK|SELECT|SET|TRUNCATE|UNLOAD|UNSET|UPDATE|UPSERT|WITH|alter|begin|call|comment|commit|connect|create|delete|drop|end|explain|export|grant|import|insert|load|lock|merge|refresh|rename|replace|revoke|rollback|select|set|truncate|unload|unset|update|upsert|with).*")
  (#offset! @sql 0 1 0 -1)) ]]
    )
  end,
}
