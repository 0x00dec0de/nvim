return {
  "romgrk/barbar.nvim",
  dependencies = {
    "lewis6991/gitsigns.nvim",
    "nvim-tree/nvim-web-devicons",
  },
  init = function()
    vim.g.barbar_auto_setup = false
  end,
  opts = {
    clicable = true,
    tabpages = false,
    insert_at_end = true,
    icons = {
      filetype = { enable = true },
      buffer_index = true,
      button = "",
      diagnostics = {
        [vim.diagnostic.severity.ERROR] = { enabled = true },
        [vim.diagnostic.severity.WARN] = { enabled = true },
        [vim.diagnostic.severity.INFO] = { enabled = true },
        [vim.diagnostic.severity.HINT] = { enabled = true },
      },
      gitsigns = {
        added = { enabled = true, icon = "+" },
        changed = { enabled = true, icon = "~" },
        deleted = { enabled = true, icon = "-" },
      },
      modified = { button = "⟡" },
      pinned = { button = "", filename = true },
    }, -- end icons
    sidebar_filetypes = { NvimTree = true },
  }, -- end opts
  version = "^1.0.0", -- optional: only update when a new 1.x version is released
}
