return {
  "RRethy/vim-illuminate",
  event = { "InsertEnter" },
  config = function()
    require("illuminate").configure({
      delay = 1000,
    })
  end,
}
