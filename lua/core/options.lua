vim.g.closetag_filenames = "*.html,*.xhtml,*.phtml,*.svelte,*.vue"
vim.g.neovide_transparency = 0.95
vim.g.transparency = 0.88
vim.g.mapleader = " " -- !!!  <Space> - for this option not working
vim.keymap.set("n", "<Space>", "<Nop>", { silent = true })
vim.opt.timeoutlen = 700 -- time to wait for a mapped sequence to complate ( in milliseconds )

vim.opt.guifont = { "Hack Nerd Font Mono", ":h16" }
-- vim.opt.guifont = { "JetBrainsMono Nerd Font Mono", ":h16" }
-- vim.opt.guifont = { "RobotoMono Nerd Font Mono", ":h16" }
-- vim.opt.guifont = { "UbuntuMono Nerd Font Mono", ":h20" }
-- vim.opt.guifont = { "DejaVuSansM Nerd Font Mono", ":h16" }

vim.opt.signcolumn = "yes"
vim.opt.hidden = true
vim.opt.autoindent = true
vim.opt.autoread = true
vim.opt.backspace = { "indent", "eol", "start" }
vim.opt.backup = false
vim.opt.cursorline = true
vim.opt.colorcolumn = { 80, 100 }
vim.opt.completeopt = { "longest", "menuone", "preview" }
vim.opt.clipboard:append("unnamedplus") -- allow neovim to access the system clipboard
vim.opt.conceallevel = 0 -- so that `` is visible in markdown file
vim.opt.encoding = "utf-8"
vim.opt.fileencoding = "utf-8" -- the encoding write to file
vim.opt.expandtab = true -- convert tabs to spaces
vim.opt.swapfile = false
vim.opt.smarttab = true
vim.opt.smartindent = true
vim.opt.formatoptions = "tcqrn1"
vim.opt.backup = false
vim.opt.writebackup = false
vim.opt.compatible = false
vim.opt.cmdheight = 1
vim.opt.updatetime = 300 -- faster completion
vim.opt.hlsearch = true
vim.opt.ignorecase = true
vim.opt.incsearch = true
vim.opt.laststatus = 2
vim.opt.mmp = 5000
vim.opt.modelines = 2
vim.opt.mouse = "a" -- allow mouse used in neovim
vim.opt.errorbells = false
vim.opt.visualbell = true
vim.opt.shiftround = false
vim.opt.startofline = false
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.regexpengine = 0 -- if set it to 1 it breacks nvim cmp cmdline
vim.opt.ruler = true
vim.opt.scrolloff = 5
vim.opt.sidescrolloff = 5
vim.opt.shiftwidth = 2 -- the space inserted to each insertion
vim.opt.showcmd = true
vim.opt.showmatch = true
vim.opt.showmode = true
vim.opt.smartcase = true
vim.opt.softtabstop = 2
vim.opt.spelllang = "en_us"
vim.opt.splitbelow = true
vim.opt.splitright = true
vim.opt.tabstop = 2
vim.opt.ttimeout = true
vim.opt.ttyfast = true
vim.opt.undofile = true -- enable persistent undo
vim.opt.virtualedit = "block"
vim.opt.wildmenu = true
vim.opt.wildmode = "full"
vim.opt.wrap = false
vim.opt.termguicolors = true
vim.opt.filetype = "on"
vim.opt.syntax = "on"
vim.opt.shortmess:append("crtToOI")
vim.opt.complete:append("kspell")
vim.opt.whichwrap = "<,>,[,],b,s"
vim.opt.linespace = 8
vim.opt.list = false -- enable and disable listchars
--vim.opt.listchars:append "eol:↲"
--vim.opt.listchars = {eol = '↵'}

-- disable netrw at the very start of your init.lua
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- Fix markdown indentation settings
vim.g.markdown_recommended_style = 0

vim.opt.completeopt = "menu,menuone,noselect"
