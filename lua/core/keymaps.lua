-- need to be added keymap for clear searh :nohl
--

local opts = { noremap = true, silent = true }
local map = vim.keymap.set

-- nvim-tree ------------------------------------------------------------------
map("n", "<leader>ec", "<cmd>NvimTreeCollapse<CR>", { desc = "Collapse file explorer" })

-- --- barbar --------------------------------------------------------------------
-- Move to previous/next
map("n", "<S-h>", "<Cmd>BufferPrevious<CR>", opts)
map("n", "<S-l>", "<Cmd>BufferNext<CR>", opts)
-- Re-order to previous/next
map("n", "<A-<>", "<Cmd>BufferMovePrevious<CR>", opts)
map("n", "<A->>", "<Cmd>BufferMoveNext<CR>", opts)
-- Goto buffer in position...
map("n", "<A-1>", "<Cmd>BufferGoto 1<CR>", opts)
map("n", "<A-2>", "<Cmd>BufferGoto 2<CR>", opts)
map("n", "<A-3>", "<Cmd>BufferGoto 3<CR>", opts)
map("n", "<A-4>", "<Cmd>BufferGoto 4<CR>", opts)
map("n", "<A-5>", "<Cmd>BufferGoto 5<CR>", opts)
map("n", "<A-6>", "<Cmd>BufferGoto 6<CR>", opts)
map("n", "<A-7>", "<Cmd>BufferGoto 7<CR>", opts)
map("n", "<A-8>", "<Cmd>BufferGoto 8<CR>", opts)
map("n", "<A-9>", "<Cmd>BufferGoto 9<CR>", opts)
map("n", "<A-0>", "<Cmd>BufferLast<CR>", opts)
-- Pin/unpin buffer
map("n", "<A-p>", "<Cmd>BufferPin<CR>", opts)
-- Close buffer
map("n", "<A-c>", "<Cmd>BufferCloseAllButCurrentOrPinned<CR>", opts)
-- Wipeout buffer
--                 :BufferWipeout
-- Close commands
--                 :BufferClose
--                 :BufferCloseAllButCurrent
--                 :BufferCloseAllButPinned
--                 :BufferCloseAllButCurrentOrPinned
--                 :BufferCloseBuffersLeft
--                 :BufferCloseBuffersRight
--
-- Magic buffer-picking mode
map("n", "<C-p>", "<Cmd>BufferPick<CR>", opts)

-- Sort automatically by...
map("n", "<Leader>bb", "<Cmd>BufferOrderByBufferNumber<CR>", opts)
map("n", "<Leader>bn", "<Cmd>BufferOrderByName<CR>", opts)
map("n", "<Leader>bd", "<Cmd>BufferOrderByDirectory<CR>", opts)
map("n", "<Leader>bl", "<Cmd>BufferOrderByLanguage<CR>", opts)
map("n", "<Leader>bw", "<Cmd>BufferOrderByWindowNumber<CR>", opts)

-- Other:
-- :BarbarEnable - enables barbar (enabled by default)
-- :BarbarDisable - very bad command, should never be used

--- NvimTree -----------------------------------------------------------------

map("n", "<C-n>", "<Cmd>NvimTreeToggle<CR>", opts)
-- map('n', '???', '<Cmd>NvimTreeCollaplse<CR>', opts)

-- --- Telescope ----------------------------------------------------------------

map("n", "<Leader>ff", "<Cmd>Telescope find_files<CR>", opts)
map("n", "<Leader>fg", "<Cmd>Telescope live_grep<CR>", opts)
map("n", "<Leader>fb", "<Cmd>Telescope buffers<CR>", opts)
map("n", "<Leader>fh", "<Cmd>Telescope help_tags<CR>", opts)

-- map("n", "<Leader>", "<Cmd>Telescope <CR>", opts)

map("n", ";", "<Cmd>Telescope buffers<CR>", opts)
map("n", "f", "<Cmd>Telescope find_files hidden=true<CR>", opts)

map("n", "<F5>", "<Cmd>LspRestart<CR>", opts)

map("n", "<leader>a", "<cmd>AerialToggle!<CR>")

map("n", "gx", "<esc>:URLOpenUnderCursor<CR>", { nowait = true })

map("v", ">", ">gv")
map("v", "<", "<gv")

map("n", "<Esc>", "<CMD>noh<CR>")

-- Allow moving the cursor through wrapped lines with j, k, <Up> and <Down>
-- http://www.reddit.com/r/vim/comments/2k4cbr/problem_with_gj_and_gk/
-- empty mode is same as using <cmd> :map
-- also don't use g[j|k] when in operator pending mode, so it doesn't alter d, y or c behaviour
map("n", "j", 'v:count || mode(1)[0:1] == "no" ? "j" : "gj"', { expr = true })
map("n", "k", 'v:count || mode(1)[0:1] == "no" ? "k" : "gk"', { expr = true })

-- Don't copy the replaced text after pasting in visual mode
-- https://vim.fandom.com/wiki/Replace_a_word_with_yanked_text#Alternative_mapping_for_paste
-- map("n", "p", 'p:let @+=@0<CR>:let @"=@0<CR>', { silent = true })

-- VimWiki
map("n", "<F2>", "<Cmd>VimwikiIndex<CR>", opts)
